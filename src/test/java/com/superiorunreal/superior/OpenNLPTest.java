package com.superiorunreal.superior;

import com.superiorunreal.superior.domain.Message;
import com.superiorunreal.superior.domain.Result;
import com.superiorunreal.superior.service.TextFileReader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;

import static com.superiorunreal.superior.service.TextFileReader.parseMessageObject;

public class OpenNLPTest {

    @Test
    public void SentenceDetectorTest() throws IOException {

        JSONArray messageList = TextFileReader.getFileContent("fileIn/example.json");
        OpenNLP openNLP = new OpenNLP();

        ArrayList<Result> flaggedSentences = new ArrayList<>();

        messageList.forEach( emp -> {
            Message data = parseMessageObject( (JSONObject) emp );

            try {
                flaggedSentences.addAll(openNLP.extractEntitiesFromText(data));
            } catch (final IOException e) {
                e.printStackTrace();
            } catch (final NullPointerException e) {
//                e.printStackTrace();
            }
            for(Result fs : flaggedSentences) {
                System.out.println("{\n 'username': '" + fs.getUser() + "', \n 'message': '" + fs.getSentence() + "', \n 'time': '" + fs.getTimestamp() +"', \n 'flag': '" + fs.getFlag() + "' \n},");
            }
            flaggedSentences.clear();
            // Assert.assertTrue(sentences != null && sentences.size() > 0);
//            System.out.println();
        });

    }

//    @Test
//    public void superGoodFlawlessTest() {
//
//        OpenNLP openNLP = new OpenNLP();
//
//        Message msg1 = new Message("footDude",
//                "could you crack those feet out?",
//                "01/01/1970",null);
//        Message msg2 = new Message("footDude",
//                "would you please get them toes out?",
//                "01/01/2019", null);
//        Message msg3 = new Message("notPredator",
//                "I'm not a predator.","never",null);
//
//        ArrayList<Message> msgs = new ArrayList<Message>();
//        msgs.add(msg1);
//        msgs.add(msg2);
//        msgs.add(msg3);
//        try {
//            msgs = openNLP.processElasticChunkAlsoNLP(msgs);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }
}
