package com.superiorunreal.superior;

import com.superiorunreal.superior.service.MainService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ComponentScan()
public class SuperiorApplication {

	public static void main(String[] args) {

		SpringApplication.run(SuperiorApplication.class, args);

    }

}
