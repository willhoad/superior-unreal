package com.superiorunreal.superior.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.superiorunreal.superior.domain.Message;
import com.superiorunreal.superior.domain.Result;
import org.apache.http.HttpHost;
import org.apache.log4j.Logger;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

@Component
public class ElasticController {
    public static final Logger LOG = Logger.getLogger(ElasticController.class);

    String indexName = "chat";
    String type = "message";

    private RestHighLevelClient client;
    private ObjectMapper mapper;


    public ElasticController() {
        client = new RestHighLevelClient(
                RestClient.builder(new HttpHost("localhost", 9200)));

        mapper = new ObjectMapper();
    }

    public void postData(Result result) {
        try {
            Request request = new Request("POST", indexName + "/" + type);
            request.setJsonEntity(mapper.writeValueAsString(result));
            Response response = client.getLowLevelClient().performRequest(request);
            System.out.println(response.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Message getData() {
        try {
            Request request = new Request("POST", "stream/chat/_search");
            String requestJson = "{\"size\": 1, \"sort\": { \"date\": \"desc\"}, \"query\": {\"match_all\": {}}}";
            request.setJsonEntity(requestJson);
            Response response = client.getLowLevelClient().performRequest(request);

            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), Charset.defaultCharset()))) {
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder.append(line);
                }
            }
            String jsonresult = stringBuilder.toString();
            JSONParser parser = new JSONParser();
            JSONObject obj = new JSONObject(jsonresult);
            JSONObject hit = (JSONObject) obj.getJSONObject("hits").getJSONArray("hits").get(0);
            JSONObject josnmessage = hit.getJSONObject("_source");
            Message resultMessage = new Message(
                    josnmessage.getString("username"),
                    josnmessage.getString("message"),
                    josnmessage.getString("date")
            );
            return resultMessage;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


}
