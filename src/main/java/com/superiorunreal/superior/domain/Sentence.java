package com.superiorunreal.superior.domain;

import org.elasticsearch.common.Nullable;

public class Sentence {

    private String text;
    private Flags flag;


    public Sentence(String text, @Nullable Flags flag) {
        this.text = text;
        this.flag = flag;
    }

    public void setMessage(Flags flag) { this.text = text; }

    public String getMessage() { return text;}

    public void setFlag(Flags flag) { this.flag = flag; }

    public Flags getFlag() { return flag;}
}
