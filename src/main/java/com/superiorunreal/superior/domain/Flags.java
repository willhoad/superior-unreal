package com.superiorunreal.superior.domain;

public enum Flags {
    NONE, INFLUENCING, INFLUENCED, NAME, LOCATION
}
