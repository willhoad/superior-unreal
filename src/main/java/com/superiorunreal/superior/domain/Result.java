package com.superiorunreal.superior.domain;

public class Result {
    private String user;
    private String sentence;
    private String timestamp;
    private Flags flag;

    public Result(String user, String sentence, String timestamp, Flags flag) {
        this.user = user;
        this.sentence = sentence;
        this.timestamp = timestamp;
        this.flag = flag;
    }

    public String getUser() {
        return user;
    }

    public String getSentence() {
        return sentence;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public Flags getFlag() { return flag;}

}
