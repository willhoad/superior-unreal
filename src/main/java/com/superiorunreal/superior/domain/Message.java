package com.superiorunreal.superior.domain;

import org.elasticsearch.common.Nullable;

import java.util.ArrayList;


public class Message {

    private String user;
    private String chat;
    private String timestamp;
    private ArrayList<Sentence> sentences;

    public Message(String user, String chat, String timestamp, @Nullable ArrayList<Sentence> sentences) {
        this.user = user;
        this.chat = chat;
        this.timestamp = timestamp;
        if (sentences == null) {
            this.sentences = new ArrayList<>();
        } else {
            this.sentences = sentences;
        }
    }

    public Message(String user, String chat, String timestamp) {
        this.user = user;
        this.chat = chat;
        this.timestamp = timestamp;
    }



    public String getUser() {
        return user;
    }

    public String getChat() {
        return chat;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public ArrayList<Sentence> getSentence() {
        return sentences;
    }

    public void addSentences(Sentence sentence) { this.sentences.add(sentence); }

}
