package com.superiorunreal.superior.service;

import com.superiorunreal.superior.domain.Flags;
import com.superiorunreal.superior.domain.Message;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Created by jslap on 04/11/2015.
 */
public class TextFileReader {

    public static final Logger log = Logger.getLogger(TextFileReader.class);

    public static JSONArray getFileContent(String filePath) throws IOException {

//        System.out.println("Reading file contents for file: " + filePath);

        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(filePath))
        {
            //Read JSON file
            Object obj = jsonParser.parse(reader);

            JSONArray messageList = (JSONArray) obj;

            return messageList;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Message parseMessageObject(JSONObject message)
    {
        //Get employee object within list

        //Get employee first name
        String timestamp = (String) message.get("timestamp");
//        System.out.println("timestamp: " + timestamp);

        //Get employee first name
        String username = (String) message.get("username");
//        System.out.println("username: " +username);

        //Get employee last name
        String chat = (String) message.get("message");
//        System.out.println("chat: " + chat);

        Message messageObj = new Message(username, chat, timestamp, new ArrayList<>());

        return messageObj;
    }

    public static ArrayList<String> getFileContentLines(String filePath) throws IOException {

        BufferedReader abc = new BufferedReader(new FileReader(filePath));
        ArrayList<String> data = new ArrayList<>();
        String s;
        while((s=abc.readLine())!=null) {
            data.add(s);
//            System.out.println(s);
        }
        abc.close();
        return data;
    }
}
