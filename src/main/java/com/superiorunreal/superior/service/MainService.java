package com.superiorunreal.superior.service;

import com.superiorunreal.superior.OpenNLP;
import com.superiorunreal.superior.controller.ElasticController;
import com.superiorunreal.superior.domain.Message;
import com.superiorunreal.superior.domain.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;

@Component
public class MainService {

    private final static Logger LOG = LoggerFactory.getLogger(MainService.class);

    ElasticController elasticController;

    @Autowired
    public MainService(ElasticController elasticController) {
        this.elasticController = elasticController;
    }

    @Scheduled(fixedRate = 5000)
    public void loopProcessData() {
        Message message;
        OpenNLP openNLP = new OpenNLP();
        ArrayList<Result> flaggedSentences = new ArrayList<>();
        //while (true) {
        message = elasticController.getData();
        if (message != null) {
//                // Not sure how the message is being passed in but lets just presume input has the message and we make up the rest
//                Message dummyInput = new Message("exampleUser", input, "10:10:10 10:10:10:100", new ArrayList<>());
                try {
                    LOG.info("Message recieved :" + message.getChat());
                    flaggedSentences.addAll(openNLP.extractEntitiesFromText(message));


                    for(Result fs : flaggedSentences) {
                        System.out.println("{\n 'username': '" + fs.getUser() + "', \n 'message': '" + fs.getSentence() + "', \n 'time': '" + fs.getTimestamp() +"', \n 'flag': '" + fs.getFlag() + "' \n},");
                        LOG.info("Sending result");
                        elasticController.postData(fs);
                    }
                    flaggedSentences.clear();
                } catch (final IOException e) {
                    e.printStackTrace();
                } catch (final NullPointerException e) {
                    e.printStackTrace();
                }
            } else {
            message = null;
                flaggedSentences.clear();
            }
        //}
    }
}
