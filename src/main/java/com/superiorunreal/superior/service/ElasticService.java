package com.superiorunreal.superior.service;

import com.superiorunreal.superior.controller.ElasticController;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class ElasticService {
    public static final Logger LOG = Logger.getLogger(ElasticService.class);

    ElasticController elasticController;


    public ElasticService(ElasticController elasticController) {
        this.elasticController = elasticController;
    }

    public String getElasticChunk(){
        return "";
    }

    public void postResult() {
        //elasticController.postData
    }

}
