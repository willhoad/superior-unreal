package com.superiorunreal.superior;

import com.superiorunreal.superior.domain.Flags;
import com.superiorunreal.superior.domain.Message;
import com.superiorunreal.superior.domain.Result;
import com.superiorunreal.superior.domain.Sentence;
import com.superiorunreal.superior.service.TextFileReader;
import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.Span;
import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

;

public class OpenNLP {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(OpenNLP.class);

    // Models - Use these to extract data
    private SentenceDetectorME sentenceDetector;
    private TokenizerME tokenizer;
    private NameFinderME nameFinder;
    private NameFinderME locationFinder;

    public static final Logger log = Logger.getLogger(OpenNLP.class);

    /**
     * Load models in constructor
     */
    public OpenNLP() {
        InputStream inputStream;

        try {
            inputStream = new FileInputStream("models/en-sent.bin");
            SentenceModel model = new SentenceModel(inputStream);
            sentenceDetector = new SentenceDetectorME(model);
        } catch (Throwable e) {
            log.error("[ERROR] Could not find Model file for sentence detector", e);
        }

        try {
            inputStream = new FileInputStream("models/en-token.bin");
            TokenizerModel model = new TokenizerModel(inputStream);
            tokenizer = new TokenizerME(model);
        } catch (Throwable e) {
            log.error("[ERROR] Could not find Model file for Tokenizer", e);
        }

        try {
            inputStream = new FileInputStream("models/en-ner-person.bin");
            TokenNameFinderModel model = new TokenNameFinderModel(inputStream);
            nameFinder = new NameFinderME(model);
        } catch (Throwable e) {
            log.error("[ERROR] Could not find Model file for name finder", e);
        }

        try {
            inputStream = new FileInputStream("models/en-ner-location.bin");
            TokenNameFinderModel model = new TokenNameFinderModel(inputStream);
            locationFinder = new NameFinderME(model);
        } catch (Throwable e) {
            log.error("[ERROR] Could not find Model file for name finder", e);
        }
    }

    public ArrayList<Result> extractEntitiesFromText(Message incomingMessage) throws IOException {

        ArrayList<String> influences = TextFileReader.getFileContentLines("fileIn/influencing.txt");
        ArrayList<String> influencee = TextFileReader.getFileContentLines("fileIn/influencee.txt");
        // use OpenNLP to look for entities and display as you desire
        Message message = new Message(incomingMessage.getUser(), incomingMessage.getChat(), incomingMessage.getTimestamp(), new ArrayList<>());
        ArrayList<Result> flaggedSentences = new ArrayList<>();

        String sentences[] = sentenceDetector.sentDetect(message.getChat());
        for (String sent : sentences) {
            // Loop through the list of influencing phrases or words
            for (int i = 0; i < influences.size(); i++) {
                if(sent.toLowerCase().contains(influences.get(i)) || sent.contains("?")) {
                    Sentence sentence = new Sentence(sent, Flags.INFLUENCING);
                    message.addSentences(sentence);
                    break;
                }
            }
            // Loop through the list of influencee phrases or words
            for (int i = 0; i < influencee.size(); i++) {
                if(sent.toLowerCase().contains(influencee.get(i))) {
                    Sentence sentence = new Sentence(sent, Flags.INFLUENCED);
                    message.addSentences(sentence);
                    break;
                }
            }

            // tokenize
            String[] tokens = tokenizer.tokenize(sent);

            //Check if a name has been mentioned
            Span nameSpans[] = nameFinder.find(tokens);
            if(nameSpans.length > 0) {
                Sentence sentence = new Sentence(sent, Flags.NAME);
                message.addSentences(sentence);
            }

            //Check if a location has been mentioned
            Span locSpans[] = locationFinder.find(tokens);
            if(locSpans.length > 0) {
                Sentence sentence = new Sentence(sent, Flags.LOCATION);
                message.addSentences(sentence);
            }
        }

//        System.out.println("Flagged Content");
        ArrayList<Sentence> sentenceList = message.getSentence();
//        System.out.println();
        for(Sentence s : sentenceList) {

            if(s.getFlag() != Flags.NONE) {
                flaggedSentences.add(new Result(message.getUser(), s.getMessage(), message.getTimestamp(), s.getFlag()));
                LOG.info("MATCHED!!!");
            }
        }
        return flaggedSentences;
    }

//    public ArrayList<Message> processElasticChunkAlsoNLP(ArrayList<Message> messages) throws IOException {
//
//        ArrayList<String> influences = TextFileReader.getFileContentLines("fileIn/influencing.txt");
//        ArrayList<String> influencee = TextFileReader.getFileContentLines("fileIn/influencee.txt");
//        ArrayList<String> flaggedSentences = new ArrayList<>();
//
//        // use OpenNLP to look for entities and display as you desire
//
//        boolean isInfluencing, isInfluenced;
//        Flags flag;
//        double influencingScoreFactor = 0.00;
//
//        for (Message msg : messages) {
//            isInfluencing = false;
//            isInfluenced = false;
//            flag = Flags.NONE;
//
//            // Loop through the list of influencing phrases or words
//            for (int i = 0; i < influences.size(); i++) {
//                if(msg.getChat().toLowerCase().contains(influences.get(i)) && msg.getChat().contains("?")) {
//                    isInfluencing = true;
//                    flag = Flags.INFLUENCED;
//
//                }
//            }
//            // Loop through the list of influencee phrases or words
//            for (int i = 0; i < influencee.size(); i++) {
//                if(msg.getChat().toLowerCase().contains(influencee.get(i))) {
//                    isInfluenced = true;
//                    flag = Flags.INFLUENCING;
//                }
//            }
//            // Now we know this sentence has an aspect of influencing we can set it as flagged
//            if(isInfluencing || isInfluenced) {
//                flaggedSentences.add(msg.getChat());
//
//            }
//
//            // set the flag no matter what as null by default
////            msg.setFlag(flag);
////            System.out.println(msg.getFlag());
//        }
//        System.out.println("Flagged Content");
//        System.out.println(flaggedSentences);
//        System.out.println("Flagged Messages");
//        System.out.println(messages);
//        return messages;
//    }

}
