/**
 * Created by gparker.
 */

// Server Set up
var express = require("express"),
  app = express(),
  morgan = require("morgan"), // log requests to the console
  bodyParser = require("body-parser"),
  cors = require("cors"),
  methodOverride = require("method-override")
path = require("path")

// Configuration
// =====================================================================================================================

// App Level Middleware
// =====================================================================================================================

app.use(express.static(__dirname + "/public")) // set the static files location /public/img will be /img for users
app.use(cors()) // different ports on localhost need CORS
app.use(morgan("dev")) // log every request to the console
app.use(bodyParser.urlencoded({ extended: "true" })) // parse application/x-www-form-urlencoded
app.use(bodyParser.json()) // parse application/json
app.use(bodyParser.json({ type: "application/vnd.api+json" })) // parse application/vnd.api+json as json
app.use(methodOverride())

// Routing
// =====================================================================================================================

var router = require("./router")
app.use("/api", router)
app.use("/app/", express.static(path.join(__dirname, "../home-page-frontend/dist/")))

// Server Start
// =====================================================================================================================
app.listen(8883)
console.log("Mock server listening on port 8883")
