var mockData = require("./data/chat.json")
var mockSteamerData = require("./data/mockSteamerData.json")
var axios = require("axios")
var moment = require("moment")

var getChat = function(req, res) {
  let getDate = () => {
    return moment().format()
  }

  const callAxios = c => {
    axios
      .post("http://localhost:9200/stream/chat", {
        username: c.username,
        message: c.message,
        date: getDate()
      })
      .then(function(response) {
        // console.log(response)
      })
      .catch(function(error) {
        // console.log(error)
      })
  }

  mockData.forEach(function(c, index) {
    setTimeout(function() {
      res.write(c.toString())
      console.log(c.username + ":  " + c.message)
      callAxios(c)
    }, 1000 * index)
    if (c === mockData.length) {
      res.end()
    }
  })
}

var getStreamer = function(req, res) {
  let getDate = () => {
    return moment().format()
  }

  const callAxios = c => {
    axios
      .post("http://localhost:9200/streamer/action", {
        username: c.username,
        message: c.message,
        date: getDate()
      })
      .then(function(response) {})
      .catch(function(error) {})
  }

  mockSteamerData.forEach(function(c, index) {
    setTimeout(function() {
      res.write(c.toString())
      console.log(c.message)

      callAxios(c)
    }, 5000 * index)
    if (c === mockSteamerData.length) {
      res.end()
    }
  })
}

var ChatAPI = {
  get: getChat,
  getStreamer: getStreamer
}

module.exports = ChatAPI
