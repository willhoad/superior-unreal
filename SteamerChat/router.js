// Server
var express = require("express")
var router = express.Router()

// Controllers
var chat = require("./ChatController")

// Mock API
router.route("/getChat").get(chat.get)
router.route("/getStreamerChat").get(chat.getStreamer)

module.exports = router
